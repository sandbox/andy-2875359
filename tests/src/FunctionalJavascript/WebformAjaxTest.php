<?php

/**
 * @todo Try to remove calls to JSWebAssert::assertWaitOnAjaxRequest() by adding
 *   specific HTML classes to the fields/confirmation
 * @todo Reorder tests.
 * @todo Expand test coverage:
 *   - multiple step forms with preview and save draft enabled;
 *   - server-side validation.
 */

namespace Drupal\Tests\webform_ajax\FunctionalJavascript;

use Drupal\Component\Utility\Random;
use Drupal\Core\Url;
use Drupal\FunctionalJavascriptTests\JavascriptTestBase;

/**
 * Test AJAX submissions.
 *
 * @group webform_ajax
 *
 * @todo This lumps a number of different tests together. AJAX is quite a cross-
 *   cutting concern and I'd welcome input on how best to slice up the tests.
 */
class WebformAjaxTest extends JavascriptTestBase {

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  public static $modules = ['webform_ajax'];

  /**
   * @var \Drupal\Tests\webform_ajax\FunctionalJavascript\WebformProvider $provider
   */
  protected $provider;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->provider = new WebformAjaxProvider();
  }

  /**
   * Test AJAX webform submission storage.
   *
   * @see \Drupal\webform\Tests\WebformSubmissionStorageTest::testSubmissionStorage()
   */
  public function testAjaxSubmissionStorage() {
      $webform = $this->provider->webformWithTextfield();
      $webform->setSetting('confirmation_type', 'message');
      $webform->save();
      $assert = $this->assertSession();

      $this->drupalPostForm($webform->toUrl(), ['textfield' => 'test value'], t('Submit'));
      $assert->waitForElement('css', '.messages--status');
      /** @var \Drupal\webform\WebformSubmissionStorageInterface $storage */
      $storage = \Drupal::entityTypeManager()->getStorage('webform_submission');

      $this->assertEquals($storage->getTotal($webform), 1);
  }

  /**
   * Test server-side validation of required fields.
   *
   * @todo Look into why client-side validation doesn't seem to kick in.
   */
  public function testRequiredTextField() {
    $webform = $this->provider->webformWithTextfield(['#required' => TRUE]);
    $webform->setSetting('confirmation_type', 'message');
    $webform->save();
    $assert = $this->assertSession();

    $this->drupalPostForm($webform->toUrl(), ['textfield' => ''], t('Submit'));
    $element = $assert->waitForElement('css', '.messages--error');

    $this->assertNotNull($element, "An AJAX submission triggers server-side validation errors.");
    $assert->pageTextContains('Textfield field is required.');
  }

  /**
   * Test an AJAX submission with a page confirmation.
   */
  public function testAjaxSubmissionPageConfirmation() {
    $webform = $this->provider
      ->webformWithTextfield()
      ->setSetting('confirmation_type', 'page');
    $webform->save();
    $assert = $this->assertSession();
    $expected_path = Url::fromRoute('entity.webform.confirmation', ['webform' => $webform->id()]);

    $this->drupalPostForm($webform->toUrl(), ['textfield' => 'test value'], t('Submit'));
    $confirmation = $assert->waitForElement('css', '.webform-confirmation');
    $actual_path  = parse_url($this->getUrl(), PHP_URL_PATH);

    $this->assertNotNull($confirmation, "An AJAX submission redirects to the confirmation page.");
    $assert->pageTextContains('New submission added to Test webform');
    $assert->pageTextContains('test value');
    $this->assertEquals($actual_path, $expected_path->toString());

    $this->clickLink('Back to form');
    $textfield = $assert->waitForField('Textfield');
    $this->assertNotNull($textfield, "Clicking the 'Back to form' link on the confirmation page after an AJAX submission loads the form.");
  }

  /**
   * Test an AJAX submission with inline confirmation.
   */
  public function testAjaxSubmissionInlineConfirmation() {
    // For now use the test_element_text webform as an easy check.
    $webform = $this->provider->webformWithTextfield();
    $webform->setSetting('confirmation_type', 'inline');
    $webform->save();
    // @todo Use an API call to get the form ID.
    $form_id = 'webform_submission_' . $webform->id() . '_form';
    // The inline confirmation message is wrapped in the same wrapper the form
    // was.
    // @todo Use an API to get the prefix.
    $locator = "#webform-ajax-$form_id .webform-confirmation";
    $assert = $this->assertSession();

    $this->drupalPostForm($webform->toUrl()->getInternalPath(), ['textfield' => 'test value'], t('Submit'));
    $element = $assert->waitForElement('css', $locator);

    $this->assertNotNull($element, "An AJAX submission shows an inline confirmation message.");
    $assert->pageTextContains('New submission added to Test webform');
    $assert->pageTextContains('test value');

    $this->clickLink('Back to form');
    $textfield = $assert->waitForField('Textfield');
    $this->assertNotNull($textfield, "Clicking the 'Back to form' link on the confirmation page after an AJAX submission loads the form.");
  }

  /**
   * Test an AJAX submission with a message confirmation.
   */
  public function testAjaxSubmissionMessageConfirmation() {
    $webform = $this->provider->webformWithTextfield();
    $webform->setSetting('confirmation_type', 'message');
    $webform->save();
    $assert = $this->assertSession();
    $this->drupalPostForm($webform->toUrl(), ['textfield' => 'test value'], t('Submit'));
    $element = $assert->waitForElement('css', '.messages--status');

    $this->assertNotNull($element, "An AJAX submission shows a confirmation message.");
    $assert->pageTextContains('New submission added to Test webform');
    $assert->pageTextContains('test value');
    $assert->fieldValueNotEquals('textfield', 'test value');
  }

  /**
   * Test an AJAX submission with a URL confirmation.
   */
  public function testAjaxSubmissionUrlConfirmation() {
    $webform = $this->provider
      ->webformWithTextfield()
      ->setSetting('confirmation_type', 'url')
      ->setSetting('confirmation_url', '<front>');
    $webform->save();
    $assert = $this->assertSession();

    $this->drupalPostForm($webform->toUrl(), ['textfield' => 'test value'], t('Submit'));
    $element = $assert->waitForElement('css', '.messages--warning');
    $actual_path  = parse_url($this->getUrl(), PHP_URL_PATH);

    $this->assertNotNull($element, "An AJAX submission redirects to a custom URL.");
    $assert->pageTextContains('test value');
    $this->assertEquals($actual_path, '/');
  }

  /**
   * Test an AJAX submission with a URL confirmation.
   */
  public function testAjaxSubmissionUrlMessageConfirmation() {
    $webform = $this->provider
      ->webformWithTextfield()
      ->setSetting('confirmation_type', 'url_message')
      ->setSetting('confirmation_url', '<front>');
    $webform->save();
    $assert = $this->assertSession();

    $this->drupalPostForm($webform->toUrl(), ['textfield' => 'test value'], t('Submit'));
    $element = $assert->waitForElement('css', '.messages--status');
    $actual_path  = parse_url($this->getUrl(), PHP_URL_PATH);

    $this->assertNotNull($element, "An AJAX submission redirects to a custom URL.");
    $assert->pageTextContains('New submission added to Test webform');
    $assert->pageTextContains('test value');
    $this->assertEquals($actual_path, '/');
  }

  /**
   * Test two AJAX submission with a message confirmation.
   */
  public function testMultipleAjaxSubmissionMessageConfirmation() {
    $webform = $this->provider->webformWithTextfield();
    $webform->setSetting('confirmation_type', 'message');
    $webform->save();
    $assert = $this->assertSession();

    $this->drupalPostForm($webform->toUrl(), ['textfield' => 'test value'], t('Submit'));
    $assert->waitForElement('css', '.messages--status');
    $this->drupalPostForm(NULL, ['textfield' => 'test value 2'], t('Submit'));
    // I'm not sure there's an element I can wait on.
    $assert->assertWaitOnAjaxRequest();
    $element = $assert->waitForElement('css', '.messages--status');

    $this->assertNotNull($element, "An AJAX submission shows a confirmation message.");
    $assert->pageTextContains('New submission added to Test webform');
    $assert->pageTextContains('test value 2');
  }

  /**
   * Test a two page form submits on both pages.
   */
  public function testMultiPageForms() {
    $webform = $this->provider->multipageWebformWithTextfields();
    $webform->setSetting('confirmation_type', 'message');
    $webform->save();
    $assert = $this->assertSession();

    $this->drupalPostForm($webform->toUrl(), ['textfield_1' => 'test value page 1'], t('Next Page >'));
    $element = $assert->waitForButton('Submit');

    $this->assertNotNull($element, "An AJAX submission on the first page of a multi-page form causes the next page to show");
    $assert->pageTextContains('test value page 1');

    $this->drupalPostForm(NULL, ['textfield_2' => 'test value page 2'], t('Submit'));
    $element = $assert->waitForElement('css', '.messages--status');

    $this->assertNotNull($element, "An AJAX submission on the last page of a multi-page form shows the webform confirmation.");
    $assert->pageTextContains('test value page 2');
    $assert->pageTextContains('Textfield Page 1');
    $assert->pageTextNotContains('Textfield Page 2');
  }
  
  /**
   * Test that messages from submissions replace previous ones.
   */
  public function testMessages() {
    $webform = $this->provider->webformWithTextfield();
    $webform->setSetting('confirmation_type', 'message');
    $webform->save();
    $assert = $this->assertSession();

    $this->drupalPostForm($webform->toUrl(), ['textfield' => 'test value'], t('Submit'));
    $assert->waitForElement('css', '.messages--warning');
    $this->drupalPostForm(NULL, ['textfield' => 'test value 2'], t('Submit'));
    $assert->assertWaitOnAjaxRequest();
    $element = $assert->waitForElement('css', '.messages--warning');

    $this->assertNotNull($element, "An AJAX submission shows a confirmation message.");
    $assert->elementsCount('css', '.messages--warning', 1);
  }
}

/**
 * Helper object that provides webforms for tests.
 *
 * @todo: Move this to a sensible namespace (but where?)
 * @todo: Add some helper methods for adding elements to a webform. I think it
 *   will be neater than FAPI arrays into ::createWebform().
 */
class WebformProvider {
  /**
   * Create a webform with a textfield.
   *
   * The webform title is 'Test webform' and the field name is 'textfield'. The
   * default textfield title is 'Textfield' but that can be overridden and
   * further properties provided with $properties.
   *
   * @param array $properties
   *   Properties for the textfield; they will override any defaults.
   *
   * @return \Drupal\webform\WebformInterface
   */
  public function webformWithTextfield($properties = []) {
    return $this->createWebform([
      'title' => 'Test webform',
      'elements' => [
        'textfield' => $this->textfield($properties),
      ]
    ]);
  }

  /**
   * Create a two page webform with a textfield on each page.
   *
   * It uses the following structure.
   *   page_1 (Page 1):
   *     - textfield_1 (Textfield 1)
   *   page_2 (Page 2):
   *     - textfield_2 (Textfield 2)
   *
   * @return \Drupal\webform\WebformInterface
   */
  public function multipageWebformWithTextfields() {
    return $this->createWebform([
      'title' => 'Test webform',
      'elements' => [
        'page_1' => [
          '#type' => 'webform_wizard_page',
          '#title' => 'Page 1',
          'textfield_1' => $this->textfield(['#title' => 'Textfield Page 1']),
        ],
        'page_2' => [
          '#type' => 'webform_wizard_page',
          '#title' => 'Page 2',
          'textfield_2' => $this->textfield(['#title' => 'Textfield Page 2']),
        ]
      ]
    ]);
  }

  /**
   * Create a textfield FAPI array, optionally with custom properties.
   *
   * By default the title is 'Textfield'.
   *
   * @param array $properties
   *   Custom properties that take precedence over the defaults.
   *
   * @return array
   *   The FAPI array
   */
  public function textfield($properties = []) {
    return $properties + [
      '#type' => 'textfield',
      '#title' => 'Textfield',
    ];
  }

  /**
   * Create a new webform entity.
   *
   * @param array $values
   *   The initial values to pass to the Webform constructor; if you set the
   *   settings key directly here then no defaults will be provided (not
   *   recommended).
   *
   * @return \Drupal\webform\WebformInterface
   */
  public function createWebform($values = []) {
    $values += [
      'handlers' => [
        'debug' => [
          'id' => 'debug',
          'label' => 'Debug',
          'handler_id' => 'debug',
          'status' => TRUE,
          'weight' => 1,
          'settings' => [],
        ]
      ]
    ];

    if (empty($values['id'])) {
      $random = new Random();
      $values['id'] = $random->word(8);
    }
    $storage = \Drupal::entityTypeManager()->getStorage('webform');
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $storage->create($values);
    return $webform;
  }
}

/**
 * Helper object that provides webforms for AJAX tests.
 *
 * @todo Move this somewhere sensible as well.
 */
class WebformAjaxProvider extends WebformProvider {
  /**
   * {@inheritdoc}
   */
  public function createWebform($values = []) {
    $webform = parent::createWebform($values);
    $webform->setThirdPartySetting('webform_ajax', 'enabled', TRUE);
    return $webform;
  }
}
