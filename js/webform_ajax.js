(function($) {
  Drupal.behaviors.webform_ajax = {
    attach : function(context, settings) {
      var wrapper_id = settings.webform_ajax.wrapper_id;
      var ajax_settings = {
        url: settings.webform_ajax.url,
        event: 'click',
        progress: {
          type: 'throbber'
        }
      };

      // Bind Ajax behaviors to Webform confirmation screen's "Go back to form"
      // link.
      $(context)
        .find('.webform-confirmation__back a')
        .once('webform_ajax')
        .each(function() {
        Drupal.Ajax[wrapper_id] = new Drupal.Ajax(wrapper_id, this, ajax_settings);
      });
    }
  };
}(jQuery));
